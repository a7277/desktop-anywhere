# Desktop Anywhere
This repo houses the Desktop Anywhere lesson and scripts necessary for automated installs.

## How to use Desktop Anywhere on a Chromebook
Use these intructions to get you Desktop Anywhere running on a Chromebook

### Prerequisites
Your Chromebook must support Chromebook's Linux development environment, you can see a list of supported Chromebooks [here](https://www.chromium.org/chromium-os/chrome-os-systems-supporting-linux)

_Note: All devices launched in 2019 and afterwards will support Linux_

To turn on Linux on your Chromebook, [follow these instructions](https://support.google.com/chromebook/answer/9145439?hl=en)

### Installation
Clone this repository in the Chromebook Linux terminal

```
git clone git@gitlab.com:90cos/public/desktop-anywhere.git
```
