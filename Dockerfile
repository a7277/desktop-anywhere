FROM ubuntu:18.04
RUN { echo "[INFO] Updating system package cache and installing dependencies." && \
      apt-get update && \
      apt-get install -y sudo bash shellcheck usbutils vim && \
      echo "[INFO] Adding horizon user and setting up their home directory." && \
      useradd -s /bin/bash horizon && \
      mkdir -p /home/horizon/ && \
      chown horizon:horizon /home/horizon/; };
ADD . /workdir
WORKDIR /workdir
RUN { echo "[INFO] Setting permissions on required files." && \
      chmod u+x ./ubuntu.sh && \
      chmod u+x ./entrypoint.sh; };
ENTRYPOINT /bin/bash -c "/workdir/entrypoint.sh"
