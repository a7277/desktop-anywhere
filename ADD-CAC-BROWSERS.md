## Support cac in chrome and other browsers through nssdb

### Chrome
Only Google Chrome (https://chrome.google.com) is supported. 
Chromium is now installed as a Snap package and its sandboxing prevents it from loading the external 
libraries needed to interface with Smart Cards.


```bash
# Add OpenSC as a new PKCS #11 library to the security database
modutil -dbdir sql:$HOME/.pki/nssdb/ -add "CAC Module" -libfile /usr/lib/x86_64-linux-gnu/opensc-pkcs11.so

# Check that "CAC Module" appears in the device list
modutil -dbdir sql:$HOME/.pki/nssdb/ -list

# Add the DoD certificates
./addcerts-chrome.sh

# Check that DoD and CAC certificates are loaded
certutil -L -d sql:$HOME/.pki/nssdb/ -h all
```

### Firefox

```
profile_dir=$(dirname ~/.mozilla/firefox/*/cert9.db)

# Add OpenSC as a new PKCS #11 library to the security database
modutil -dbdir "$profile_dir" -add "CAC Module" -libfile /usr/lib/x86_64-linux-gnu/opensc-pkcs11.so

# Check that "CAC Module" appears in the device list
modutil -dbdir "$profile_dir" -list

# Add the DoD certificates
./addcerts-firefox.sh

# Check that DoD and CAC certificates are loaded
certutil -L -d "$profile_dir" -h all
```
