#!/usr/bin/env bash

parse_options() {
    if [[ "${*}" =~ --lint-only ]]; then
       echo -e "\n[INFO] User has indicated that this will be a linting only test iteration.";
       lint_only=true;
       return 0;
    else
       lint_only=false;
    fi;
    if [[ "${*}" =~ --install ]]; then
	echo -e "\n[INFO] User has indicated that this test iteration will include the install step.";
	include_install=true;
    elif [[ "${*}" =~ --no-install ]]; then
	echo -e "\n[INFO] User has indicated that this test iteration will not include the install step.";
	include_install=false;
    else
	echo -e "\n[INFO] User has not indicated if this test iteration will include the install step; defaulting to including the install step..";
	include_install=true;
    fi;
    if [[ "${*}" =~ --reader ]]; then
        echo -e "\n[INFO] User has indicated that a smart card reader should be attached during this testing iteration.";
        test_for_reader=true;
	return 0;
    elif [[ "${*}" =~ --no-reader ]]; then
        echo -e "\n[INFO] User has indicated that no smart card reader should be attached during this testing iteration.";
        test_for_reader=false;
        reader_attached=false;
	return 0;
    else
        echo -e "\n[WARN] User has not indicated whether a smart card reader should be attached during this testing iteration; not checking for reader attachment." 1>&2;
        test_for_reader=false;
        reader_attached=false;
	return 0;
    fi;
};

lint_scripts() {
    local scripts=( entrypoint.sh \
	            ubuntu.sh );
    for script in "${scripts[@]}"; do
	echo -e "\n[INFO] Linting script ($script) with shellcheck.";
	{ shellcheck "${script}" && echo -e "\n[INFO] Linting script ($script) passed."; } || \
        { echo -e "\n[FAIL] Liniting script ($script) failed." && return 1; };
    done;
};

check_euid() {
    if [[ $EUID != 0 ]]; then
        echo -e "\n[FAIL] Must be run as root and/or superuser."
    fi;
}; 

run_installer() {
    echo -e "\n[INFO] Executing the installer script (ubuntu.sh).";
    { ./ubuntu.sh -y && echo -e "\n[INFO] Installer script (ubuntu.sh) executed successfully."; } || \
    { echo -e "\n[FAIL] Installer script (ubuntu.sh) did not execute successfully." 1>&2; return 1; };
};

start_pcscd() {
    echo -e "\n[INFO] Attempting to start pcscd.";
    pcscd;
    desired_keywords="1"
    parsed_keywords="$(pgrep pcscd 2>&1 | wc -l)";
    if [[ "${parsed_keywords}" =~ ${desired_keywords} ]]; then
        echo -e "\n[INFO] Successfully started pcscd.";
        return 0;
    else
        echo -e "\n[FAIL] Failed to start pcscd; exiting.";
        return 1;
    fi;
};

get_reader_info() {
    echo -e "\n[INFO] Gathering information about attached usb devices and checking for smart card reader(s).";
    desired_keywords="Smart";
    parsed_keywords="$(lsusb 2>&1 | grep "$desired_keywords")";
    if [[ "${parsed_keywords}" =~ ${desired_keywords} ]]; then
        echo -e "\n[INFO] Smart card reader attached.";
	reader_attached=true;
	return 0;
    else
	echo -e "\n[FAIL] Smart card reader does not appear to be attached, but should be; exiting." 1>&2;
	return 1;
    fi;
};

start_cli() {
    echo -e "\n[INFO] Clearing out any old logs from previous tests prior to this testing iteration."
    rm -fr /tmp/vmware-*;
    if $reader_attached; then
        echo -e "\n[INFO] Performing test run of vmware-view, the VMWare Horizon client software."
        sudo su -c 'timeout 60 vmware-view &>/home/horizon/vmware-view-test.log' horizon
	echo -e "\n[INFO] Timeout set to 60 seconds to give the user the chance to interactively authenticate, if they wish to (not required)."
        desired_keywords="Waiting for smartcard thread to terminate";
        parsed_keywords="$(grep "$desired_keywords" /tmp/vmware-horizon/vmware-horizon-client-* 2>&1)";
	if [[ "${parsed_keywords}" =~ ${desired_keywords} ]]; then
	    echo -e "\n[INFO] Test passed; vmware-view is starting and able to access smart card reader.";
	    return 0;
	else
            echo -e "\n[INFO] Contents of vmware horizon client log(s):"
	    for log in /tmp/vmware-*/vmware-horizon-client-*; do
		echo "===== $log =====";
		cat "$log";
	    done;
	    echo -e "\n[FAIL] Test failed; vmware-view either did not start or is unable to access smart card reader; see output above." 1>&2;
	    return 1;
	fi;
    elif ! $reader_attached; then
        echo -e "\n[INFO] Performing test run of vmware-view, the VMWare Horizon client software."
        sudo su -c 'timeout 15 vmware-view &>/home/horizon/vmware-view-test.log' horizon
        echo -e "\n[INFO] Timeout set to 15 seconds to give vmware-view the chance to attempt to start successfully."
        desired_keywords="Linux Client is running";
        parsed_keywords="$(grep "$desired_keywords" /tmp/vmware-horizon/vmware-horizon-client-* 2>&1)";
	if [[ "${parsed_keywords}" =~ ${desired_keywords} ]]; then
	    echo -e "\n[INFO] Contents of vmware horizon client log(s):"
	    for log in /tmp/vmware-*/vmware-horizon-client-*; do
		echo "===== $log =====";
		cat "$log";
	    done;
            echo -e "\n[INFO] Test passed; see output above."
	    return 0;
	else
            echo -e "\n[INFO] Contents of vmware horizon client log(s):"
	    cat /tmp/vmware-*/vmware-horizon-client-*;
	    echo -e "\n[FAIL] Test failed; vmware-view did not start; see output above." 1>&2;
            return 1;
	fi;
    fi;
};

main() {
    parse_options "${@}" && \
    if ${lint_only}; then 
        lint_scripts && return 0;
    fi;
    lint_scripts && \
    check_euid && \
    if ${include_install}; then
        run_installer;
    fi && \
    start_pcscd && \
    if $test_for_reader; then
        get_reader_info;
    fi && \
    start_cli;
};

{ main "${@}" && echo -e "\n[INFO] Script encountered no fatal errors; exiting zero."; } || \
{ echo -e "\n[FAIL] Script encountered a fatal error; exiting non-zero."; exit 1; };

